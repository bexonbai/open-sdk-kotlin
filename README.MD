### Start
build.gradle.kts

Add repository.

```kotlin
repositories {
    maven("https://bitbucket.org/bexonbai/maven/raw/master/")
}
```

Add dependency.

```kotlin
dependencies {
    implementation("com.google.code.gson:gson:2.8.7")
    implementation("me.nanoleaf.opensdk:open-sdk-kotlin:1.0.0")
}
```

### Use

```kotlin
Nanoleaf.getAuthorization("10.0.243.5") { authorization, error ->
    if (authorization != null) {
        print(authorization)
    }
    if (error != null) {
        print(error)
    }
}

```