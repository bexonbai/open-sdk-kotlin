package me.nanoleaf.opensdk.models

import com.google.gson.annotations.SerializedName

data class Effects (

	@SerializedName("effectsList") val effectsList : List<String>,
	@SerializedName("select") val select : String
)