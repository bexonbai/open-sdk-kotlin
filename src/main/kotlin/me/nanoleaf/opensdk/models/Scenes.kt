package me.nanoleaf.opensdk.models

import com.google.gson.annotations.SerializedName

data class Scenes(

    @SerializedName("effectsList") var effectsList: List<String>
)