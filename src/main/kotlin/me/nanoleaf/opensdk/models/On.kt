package me.nanoleaf.opensdk.models

import com.google.gson.annotations.SerializedName

data class On (

	@SerializedName("value") val value : Boolean
)