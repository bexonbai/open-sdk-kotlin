package me.nanoleaf.opensdk.models


import com.google.gson.annotations.SerializedName

data class Layout (

	@SerializedName("numPanels") val numPanels : Int,
	@SerializedName("sideLength") val sideLength : Int,
	@SerializedName("positionData") val positionData : List<PositionData>
)