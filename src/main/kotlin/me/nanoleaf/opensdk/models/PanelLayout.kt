package me.nanoleaf.opensdk.models

import com.google.gson.annotations.SerializedName

data class PanelLayout (

	@SerializedName("globalOrientation") val globalOrientation : GlobalOrientation,
	@SerializedName("layout") val layout : Layout
)