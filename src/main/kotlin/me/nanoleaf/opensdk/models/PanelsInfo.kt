package me.nanoleaf.opensdk.models

import com.google.gson.annotations.SerializedName

data class PanelsInfo (

	@SerializedName("name") val name : String,
	@SerializedName("serialNo") val serialNo : String,
	@SerializedName("manufacturer") val manufacturer : String,
	@SerializedName("firmwareVersion") val firmwareVersion : String,
	@SerializedName("hardwareVersion") val hardwareVersion : String,
	@SerializedName("model") val model : String,
	@SerializedName("discovery") val discovery : Discovery,
	@SerializedName("effects") val effects : Effects,
	@SerializedName("firmwareUpgrade") val firmwareUpgrade : FirmwareUpgrade,
	@SerializedName("panelLayout") val panelLayout : PanelLayout,
	@SerializedName("schedules") val schedules : Schedules,
	@SerializedName("state") val state : State
)