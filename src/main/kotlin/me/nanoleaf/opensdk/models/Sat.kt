package me.nanoleaf.opensdk.models

import com.google.gson.annotations.SerializedName

data class Sat (

	@SerializedName("value") val value : Int,
	@SerializedName("max") val max : Int,
	@SerializedName("min") val min : Int
)