package me.nanoleaf.opensdk.models

import com.google.gson.annotations.SerializedName

data class PositionData (

	@SerializedName("panelId") val panelId : Int,
	@SerializedName("x") val x : Int,
	@SerializedName("y") val y : Int,
	@SerializedName("o") val o : Int,
	@SerializedName("shapeType") val shapeType : Int
)