package me.nanoleaf.opensdk.models

import com.google.gson.annotations.SerializedName

data class Authorization(
    @SerializedName("auth_token")
    var authToken: String
)