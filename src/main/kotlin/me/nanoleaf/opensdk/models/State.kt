package me.nanoleaf.opensdk.models

import com.google.gson.annotations.SerializedName

data class State (

	@SerializedName("brightness") val brightness : Brightness,
	@SerializedName("colorMode") val colorMode : String,
	@SerializedName("ct") val ct : Ct,
	@SerializedName("hue") val hue : Hue,
	@SerializedName("on") val on : On,
	@SerializedName("sat") val sat : Sat
)