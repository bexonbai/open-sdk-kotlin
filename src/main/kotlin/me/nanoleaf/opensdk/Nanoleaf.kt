package me.nanoleaf.opensdk

import me.nanoleaf.opensdk.models.*
import me.nanoleaf.opensdk.utils.GsonUtil

class Nanoleaf {
    companion object {

        fun getAuthorization(ip: String, callback: (authorization: Authorization?, error: Throwable?) -> Unit) {
            Network {
                url = "http://$ip:16021/api/v1/new"
                method = "POST"
                onSuccess { response, _ ->
                    val authorization = GsonUtil.instance.gson.fromJson(response, Authorization::class.java)
                    if (authorization != null) {
                        callback(authorization, null)
                    } else {
                        callback(null, NullPointerException("Authorization is null."))
                    }
                }
                onFail { error, responseCode ->
                    callback(null, Exception("$responseCode, $error"))
                }
            }
        }

        fun getAllPanelsInfo(ip: String, token: String, callback: (info: PanelsInfo?, error: Throwable?) -> Unit) {
            Network {
                url = "http://$ip:16021/api/v1/$token/"
                method = "GET"
                onSuccess { response, _ ->
                    val info = GsonUtil.instance.gson.fromJson(response, PanelsInfo::class.java)
                    if (info != null) {
                        callback(info, null)
                    } else {
                        callback(null, NullPointerException("PanelsInfo is null."))
                    }
                }
                onFail { error, responseCode ->
                    callback(null, Exception("$responseCode, $error"))
                }
            }
        }

        fun getState(ip: String, token: String, callback: (state: State?, error: Throwable?) -> Unit) {
            Network {
                url = "http://$ip:16021/api/v1/$token/state"
                method = "GET"
                onSuccess { response, _ ->
                    val state = GsonUtil.instance.gson.fromJson(response, State::class.java)
                    if (state != null) {
                        callback(state, null)
                    } else {
                        callback(null, NullPointerException("State is null."))
                    }
                }
                onFail { error, responseCode ->
                    callback(null, Exception("$responseCode, $error"))
                }
            }
        }

        fun getOnOff(ip: String, token: String, callback: (on: On?, error: Throwable?) -> Unit) {
            Network {
                url = "http://$ip:16021/api/v1/$token/state/on"
                method = "GET"
                onSuccess { response, _ ->
                    val on = GsonUtil.instance.gson.fromJson(response, On::class.java)
                    if (on != null) {
                        callback(on, null)
                    } else {
                        callback(null, NullPointerException("On is null."))
                    }
                }
                onFail { error, responseCode ->
                    callback(null, Exception("$responseCode, $error"))
                }
            }
        }

        fun setOnOff(ip: String, token: String, on: Boolean, callback: (error: Throwable?) -> Unit) {
            Network {
                url = "http://$ip:16021/api/v1/$token/state/on"
                method = "PUT"
                body = "{ \"on\": { \"value\": $on } }"
                onSuccess { _, _ ->
                    callback(null)
                }
                onFail { error, responseCode ->
                    callback(Exception("$responseCode, $error"))
                }
            }
        }

        fun getCT(ip: String, token: String, callback: (ct: Ct?, error: Throwable?) -> Unit) {
            Network {
                url = "http://$ip:16021/api/v1/$token/state/ct"
                method = "GET"
                onSuccess { response, _ ->
                    val ct = GsonUtil.instance.gson.fromJson(response, Ct::class.java)
                    if (ct != null) {
                        callback(ct, null)
                    } else {
                        callback(null, NullPointerException("Ct is null."))
                    }
                }
                onFail { error, responseCode ->
                    callback(null, Exception("$responseCode, $error"))
                }
            }
        }

        fun setCT(
            ip: String,
            token: String,
            cct: Int,
            brightness: Int,
            brightnessDuration: Int = 30,
            onOff: Boolean,
            callback: (error: Throwable?) -> Unit
        ) {
            Network {
                url = "http://$ip:16021/api/v1/$token/state/"
                method = "PUT"
                body =
                    "{\"ct\" : {\"increment\": $cct}, \"brightness\" : {\"value\": $brightness, \"duration\": $brightnessDuration }, \"on\" : {\"value\": $onOff } }"
                onSuccess { _, _ ->
                    callback(null)
                }
                onFail { error, responseCode ->
                    callback(Exception("$responseCode, $error"))
                }
            }
        }

        fun getHue(ip: String, token: String, callback: (hue: Hue?, error: Throwable?) -> Unit) {
            Network {
                url = "http://$ip:16021/api/v1/$token/state/hue"
                method = "GET"
                onSuccess { response, _ ->
                    val hue = GsonUtil.instance.gson.fromJson(response, Hue::class.java)
                    if (hue != null) {
                        callback(hue, null)
                    } else {
                        callback(null, NullPointerException("Hue is null."))
                    }
                }
                onFail { error, responseCode ->
                    callback(null, Exception("$responseCode, $error"))
                }
            }
        }

        fun getSaturation(ip: String, token: String, callback: (sat: Sat?, error: Throwable?) -> Unit) {
            Network {
                url = "http://$ip:16021/api/v1/$token/state/sat"
                method = "GET"
                onSuccess { response, _ ->
                    val sat = GsonUtil.instance.gson.fromJson(response, Sat::class.java)
                    if (sat != null) {
                        callback(sat, null)
                    } else {
                        callback(null, NullPointerException("Saturation is null."))
                    }
                }
                onFail { error, responseCode ->
                    callback(null, Exception("$responseCode, $error"))
                }
            }
        }

        fun setHSV(
            ip: String,
            token: String,
            hue: Int,
            sat: Int,
            brightness: Int,
            brightnessDuration: Int = 30,
            onOff: Boolean,
            callback: (error: Throwable?) -> Unit
        ) {
            Network {
                url = "http://$ip:16021/api/v1/$token/state/"
                method = "PUT"
                body =
                    "{\"hue\" : {\"increment\": $hue}, \"sat\" : {\"increment\": $sat}, \"brightness\" : {\"value\": $brightness, \"duration\": $brightnessDuration }, \"on\" : {\"value\": $onOff } }"
                onSuccess { _, _ ->
                    callback(null)
                }
                onFail { error, responseCode ->
                    callback(Exception("$responseCode, $error"))
                }
            }
        }

        fun getBrightness(ip: String, token: String, callback: (brightness: Brightness?, error: Throwable?) -> Unit) {
            Network {
                url = "http://$ip:16021/api/v1/$token/state/sat"
                method = "GET"
                onSuccess { response, _ ->
                    val brightness = GsonUtil.instance.gson.fromJson(response, Brightness::class.java)
                    if (brightness != null) {
                        callback(brightness, null)
                    } else {
                        callback(null, NullPointerException("Brightness is null."))
                    }
                }
                onFail { error, responseCode ->
                    callback(null, Exception("$responseCode, $error"))
                }
            }
        }

        fun setBrightness(
            ip: String,
            token: String,
            brightness: Int,
            brightnessDuration: Int = 30,
            on: Boolean,
            callback: (error: Throwable?) -> Unit
        ) {
            Network {
                url = "http://$ip:16021/api/v1/$token/state/"
                method = "PUT"
                body =
                    "{\"brightness\" : {\"value\": $brightness, \"duration\": $brightnessDuration}, \"on\" : {\"value\": $on}}"
                println(body)
                onSuccess { _, _ ->
                    callback(null)
                }
                onFail { error, responseCode ->
                    callback(Exception("$responseCode, $error"))
                }
            }
        }

        fun getScenes(ip: String, token: String, callback: (scenes: Scenes?, error: Throwable?) -> Unit) {
            Network {
                url = "http://$ip:16021/api/v1/$token/effects"
                method = "GET"
                onSuccess { response, _ ->
                    val scenes = GsonUtil.instance.gson.fromJson(response, Scenes::class.java)
                    if (scenes != null) {
                        callback(scenes, null)
                    } else {
                        callback(null, NullPointerException("Scenes is null."))
                    }
                }
                onFail { error, responseCode ->
                    callback(null, Exception("$responseCode, $error"))
                }
            }
        }

        fun setScene(
            ip: String,
            token: String,
            sceneName: String,
            callback: (error: Throwable?) -> Unit
        ) {
            Network {
                url = "http://$ip:16021/api/v1/$token/effects/"
                method = "PUT"
                body = "{\"select\" : \"$sceneName\"}"
                println(body)
                onSuccess { _, _ ->
                    callback(null)
                }
                onFail { error, responseCode ->
                    callback(Exception("$responseCode, $error"))
                }
            }
        }
    }
}