package me.nanoleaf.opensdk

import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL
import java.nio.charset.StandardCharsets

class Network {
    var url: String? = null
    var method: String = "GET"
    var body: String = ""

    internal var success: (String, Int) -> Unit = { _: String, _: Int -> }
    internal var fail: (Throwable, Int) -> Unit = { _: Throwable, _: Int -> }

    constructor()

    constructor(init: Network.() -> Unit) {
        val network = Network()
        network.init()
        executeForResult(network)
    }

    private fun executeForResult(network: Network) {
        val obj = URL(network.url)
        val con: HttpURLConnection = obj.openConnection() as HttpURLConnection
        when (network.method) {
            "get", "Get", "GET" -> {
                con.setRequestProperty("Accept", "*/*")
                con.setRequestProperty("User-Agent", "openSDKKotlin/1.0.0")
                con.setRequestProperty("Connection", "keep-alive")
                con.requestMethod = "GET"
            }
            "post", "Post", "POST" -> {
                con.setRequestProperty("Content-Type", "application/json; utf-8")
                con.setRequestProperty("Accept", "application/json")
                con.setRequestProperty("User-Agent", "openSDKKotlin/1.0.0")
                con.requestMethod = "POST"
                con.doOutput = true
                val outputStream = con.outputStream
                outputStream.write(network.body.toByteArray(StandardCharsets.UTF_8))
                outputStream.flush()
                outputStream.close()
            }
            "put", "Put", "PUT" -> {
                con.setRequestProperty("Content-Type", "application/json; utf-8")
                con.setRequestProperty("Accept", "application/json")
                con.setRequestProperty("User-Agent", "openSDKKotlin/1.0.0")
                con.requestMethod = "PUT"
                con.doOutput = true
                val outputStream = con.outputStream
                outputStream.write(network.body.toByteArray(StandardCharsets.UTF_8))
                outputStream.flush()
                outputStream.close()
            }
        }

        val responseCode: Int = con.responseCode
        val reader = BufferedReader(InputStreamReader(con.inputStream))
        var inputLine: String?
        val response = StringBuffer()
        while (reader.readLine().also { inputLine = it } != null) {
            response.append(inputLine)
        }
        reader.close()
        if (con.errorStream != null || responseCode == 400 || responseCode == 404 || responseCode == 500) {
            network.fail(Throwable(con.errorStream.toString()), responseCode)
        } else {
            network.success(response.toString(), responseCode)
        }
    }

    fun onSuccess(onSuccess: (response: String, responseCode: Int) -> Unit) {
        success = onSuccess
    }

    fun onFail(onFail: (error: Throwable, responseCode: Int) -> Unit) {
        fail = onFail
    }
}