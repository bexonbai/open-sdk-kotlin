package me.nanoleaf.opensdk.utils

import com.google.gson.Gson

class GsonUtil private constructor(){

    val gson = Gson()

    companion object {
        val instance: GsonUtil by lazy(mode = LazyThreadSafetyMode.SYNCHRONIZED) {
            GsonUtil()
        }
    }
}