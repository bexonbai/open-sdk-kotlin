package me.nanoleaf.opensdk.test

import me.nanoleaf.opensdk.Nanoleaf
import me.nanoleaf.opensdk.Nanoleaf.Companion.getAuthorization
import org.junit.Test

class FuncTest {

    private val ip = "10.0.243.5"
    private val token = "DZ7QU0jHdHLTJsokxHHmgc9hRXQe9VjE"

    @Test
    fun getAuthorization() {
        getAuthorization(ip) { authorization, error ->
            if (authorization != null) {
                print(authorization)
            }
            if (error != null) {
                print(error)
            }
        }
    }

    @Test
    fun getAllPanelsInfo() {
        Nanoleaf.getAllPanelsInfo(ip, token) { info, error ->
            if (info != null) {
                print(info)
            }
            if (error != null) {
                print(error)
            }
        }
    }

    @Test
    fun getState() {
        Nanoleaf.getState(ip, token) { state, error ->
            if (state != null) {
                print(state)
            }
            if (error != null) {
                print(error)
            }
        }
    }

    @Test
    fun getOnOff() {
        Nanoleaf.getOnOff(ip, token) { on, error ->
            if (on != null) {
                print(on)
            }
            if (error != null) {
                print(error)
            }
        }
    }

    @Test
    fun setOnOff() {
        Nanoleaf.setOnOff(ip, token, true) { error ->
            if (error != null) {
                print(error)
            }
        }
    }

    @Test
    fun getCt() {
        Nanoleaf.getCT(ip, token) { ct, error ->
            if (ct != null) {
                print(ct)
            }
            if (error != null) {
                print(error)
            }
        }
    }

    @Test
    fun setCT() {
        Nanoleaf.setCT(ip, token, 6000, 100, 30, true) { error ->
            if (error != null) {
                print(error)
            }
        }
    }

    @Test
    fun getHue() {
        Nanoleaf.getHue(ip, token) { hue, error ->
            if (hue != null) {
                print(hue)
            }
            if (error != null) {
                print(error)
            }
        }
    }

    @Test
    fun getSaturation() {
        Nanoleaf.getSaturation(ip, token) { sat, error ->
            if (sat != null) {
                print(sat)
            }
            if (error != null) {
                print(error)
            }
        }
    }

    @Test
    fun setHSV() {
        Nanoleaf.setHSV(ip, token, 120, 20, 50, 30, true) { error ->
            if (error != null) {
                print(error)
            }
        }
    }

    @Test
    fun getBrightness() {
        Nanoleaf.getBrightness(ip, token) { brightness, error ->
            if (brightness != null) {
                print(brightness)
            }
            if (error != null) {
                print(error)
            }
        }
    }

    @Test
    fun setBrightness() {
        Nanoleaf.setBrightness(ip, token, 100, 20, false) { error ->
            if (error != null) {
                print(error)
            }
        }
    }

    @Test
    fun getScenes() {
        Nanoleaf.getScenes(ip, token) { scenes, error ->
            if (scenes != null) {
                print(scenes)
            }
            if (error != null) {
                print(error)
            }
        }
    }

    @Test
    fun setScene() {
        Nanoleaf.setScene(ip, token, "Beatdrop") { error ->
            if (error != null) {
                print(error)
            }
        }
    }
}